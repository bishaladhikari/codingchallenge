package com.baskgroup.android.firebaseloginusingkt.ViewModel.Foctory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.baskgroup.android.firebaseloginusingkt.ViewModel.ViewModel.DetailViewModel
import com.baskgroup.android.firebaseloginusingkt.ViewModel.ViewModel.MainViewModel

class DetailViewModelFactory(): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(DetailViewModel::class.java)){
            return DetailViewModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}