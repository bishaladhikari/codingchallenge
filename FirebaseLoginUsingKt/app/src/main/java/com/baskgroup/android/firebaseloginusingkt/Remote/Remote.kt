package com.baskgroup.android.firebaseloginusingkt.Remote


import android.util.Log
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.HttpResponse
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.HttpStatus
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.ClientProtocolException
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.HttpClient
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.methods.HttpGet
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.impl.client.DefaultHttpClient
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.impl.client.HttpClientBuilder
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.URI


class Remote {

        fun requestDesert( url:String): String? {
            val httpclient: HttpClient = HttpClientBuilder.create().build();
            val response: HttpResponse
            var responseString: String? = null
            try {
                response = httpclient.execute(HttpGet(url))
                val statusLine = response.statusLine
                if (statusLine.statusCode == HttpStatus.SC_OK) {
                    val out = ByteArrayOutputStream()
                    response.entity.writeTo(out)
                    responseString = out.toString()
                    out.close()
                } else {
                    //Closes the connection.
                    response.entity.content.close()
                    throw IOException(statusLine.reasonPhrase)
                }
            } catch (e: ClientProtocolException) {
                Log.d("Error",e.toString())
            } catch (e: IOException) {
                Log.d("Error1",e.toString())
                //TODO Handle problems..
            }
            return responseString
        }



}