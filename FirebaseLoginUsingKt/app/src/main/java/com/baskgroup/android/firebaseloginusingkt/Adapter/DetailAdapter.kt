package com.baskgroup.android.firebaseloginusingkt.Adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.baskgroup.android.firebaseloginusingkt.Controller.DetailActivity
import com.baskgroup.android.firebaseloginusingkt.R
import com.bumptech.glide.Glide
import java.util.*
import kotlin.collections.ArrayList

class DetailAdapter (private val mList: List<String>) : RecyclerView.Adapter<DetailAdapter.ViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_detail, parent, false)

        return ViewHolder(view)
    }// binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val str = mList[position]

        holder.tvName.text = str


    }

    override fun getItemCount(): Int {
        return mList.size
    }
    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvName: TextView = itemView.findViewById(R.id.tvDetail)
    }


}