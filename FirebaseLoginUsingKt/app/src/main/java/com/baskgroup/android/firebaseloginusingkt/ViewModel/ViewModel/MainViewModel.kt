package com.baskgroup.android.firebaseloginusingkt.ViewModel.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.baskgroup.android.firebaseloginusingkt.Data.Item
import com.baskgroup.android.firebaseloginusingkt.Repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainViewModel : ViewModel() {

      fun getData() : ArrayList<Item> {
        val repository=Repository()
        return repository.getData()
    }
}