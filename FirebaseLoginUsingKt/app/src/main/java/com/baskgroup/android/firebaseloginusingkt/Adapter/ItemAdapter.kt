package com.baskgroup.android.firebaseloginusingkt.Adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.baskgroup.android.firebaseloginusingkt.Controller.DetailActivity
import com.baskgroup.android.firebaseloginusingkt.Data.Item
import com.baskgroup.android.firebaseloginusingkt.R
import com.bumptech.glide.Glide
import java.util.*
import kotlin.collections.ArrayList

class ItemAdapter (private val mList: List<Item>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item, parent, false)

        return ViewHolder(view)
    }// binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ItemsViewModel = mList[position]
        if (ItemsViewModel.image !== null) {
            Glide.with(holder.itemView.context)
                .load(ItemsViewModel.image)
                .error(R.drawable.ic_launcher_background)
                .into(holder.ivImage)

        } else {
            holder.ivImage.setImageResource(R.drawable.ic_launcher_background)
        }
        holder.tvName.text = ItemsViewModel.name
        holder.cv.setOnClickListener {
                val intent = Intent (holder.cv.context, DetailActivity::class.java)
            intent.putExtra("Id",""+ItemsViewModel.id)
            holder.cv.context.startActivity(intent)

        }

    }

    override fun getItemCount(): Int {
        return mList.size
    }
    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val ivImage: ImageView = itemView.findViewById(R.id.ivImage)
        val tvName: TextView = itemView.findViewById(R.id.tvName)
        val cv: CardView = itemView.findViewById(R.id.cv)
    }


}