package com.baskgroup.android.firebaseloginusingkt.Controller

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.baskgroup.android.firebaseloginusingkt.Adapter.DetailAdapter
import com.baskgroup.android.firebaseloginusingkt.Adapter.ItemAdapter
import com.baskgroup.android.firebaseloginusingkt.R
import com.baskgroup.android.firebaseloginusingkt.ViewModel.Foctory.DetailViewModelFactory
import com.baskgroup.android.firebaseloginusingkt.ViewModel.Foctory.MainViewModelFactory
import com.baskgroup.android.firebaseloginusingkt.ViewModel.ViewModel.DetailViewModel
import com.baskgroup.android.firebaseloginusingkt.ViewModel.ViewModel.MainViewModel
import com.baskgroup.android.firebaseloginusingkt.databinding.ActivityDetailBinding
import com.baskgroup.android.firebaseloginusingkt.databinding.ActivityMainBinding
import com.bumptech.glide.Glide
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService

class DetailActivity: AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.apply {
            title = "Detail"

            // show back button on toolbar
            // on back button press, it will navigate to parent activity
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            onBackPressedDispatcher
        }
        loadData();
    }
    fun loadData(){
        val id:String=intent.getStringExtra("Id").toString()
        val strIngredient: ArrayList<String> = ArrayList()
        val strMeasure: ArrayList<String> = ArrayList()
        binding.rvIngredient.layoutManager = LinearLayoutManager(this)
        binding.rvMeasurements.layoutManager = LinearLayoutManager(this)
        val factory = DetailViewModelFactory()
        val viewModel : DetailViewModel
        viewModel = ViewModelProvider(this,factory).get(DetailViewModel::class.java)
        val backgroundExecutor: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
        binding.cl.visibility=View.VISIBLE
        backgroundExecutor.execute {
            val itemDetail=viewModel.getData(id)
            itemDetail?.let{
            itemDetail.strIngredient1?.let { Log.d("ValueDe", it) }
                this@DetailActivity.runOnUiThread(java.lang.Runnable {
                    binding.tvName.text=itemDetail.strMeal
                    if (itemDetail.strMealThumb !== null) {
                        Glide.with(this)
                            .load(itemDetail.strMealThumb)
                            .error(R.drawable.ic_launcher_background)
                            .into(binding.iv)

                    } else {
                        binding.iv.setImageResource(R.drawable.ic_launcher_background)
                    }
                    binding.tvInstruction.text=itemDetail.strInstructions
                    if(!itemDetail.strIngredient1.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient1)
                    }
                    if(!itemDetail.strIngredient2.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient2)
                    }
                    if(!itemDetail.strIngredient3.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient3)
                    }
                    if(!itemDetail.strIngredient4.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient4)
                    }
                    if(!itemDetail.strIngredient5.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient5)
                    }
                    if(!itemDetail.strIngredient6.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient6)
                    }
                    if(!itemDetail.strIngredient7.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient7)
                    }
                    if(!itemDetail.strIngredient8.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient8)
                    }
                    if(!itemDetail.strIngredient9.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient9)
                    }
                    if(!itemDetail.strIngredient10.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient10)
                    }
                    if(!itemDetail.strIngredient11.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient11)
                    }
                    if(!itemDetail.strIngredient12.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient12)
                    }
                    if(!itemDetail.strIngredient13.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient13)
                    }
                    if(!itemDetail.strIngredient14.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient14)
                    }
                    if(!itemDetail.strIngredient15.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient15)
                    }
                    if(!itemDetail.strIngredient16.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient16)
                    }
                    if(!itemDetail.strIngredient17.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient17)
                    }
                    if(!itemDetail.strIngredient18.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient18)
                    }
                    if(!itemDetail.strIngredient19.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient19)
                    }
                    if(!itemDetail.strIngredient20.isNullOrEmpty()){
                        strIngredient.add(itemDetail.strIngredient20)
                    }


                    if(!itemDetail.strMeasure1.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure1)
                    }
                    if(!itemDetail.strMeasure2.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure2)
                    }
                    if(!itemDetail.strMeasure3.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure3)
                    }
                    if(!itemDetail.strMeasure4.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure4)
                    }
                    if(!itemDetail.strMeasure5.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure5)
                    }
                    if(!itemDetail.strMeasure6.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure6)
                    }
                    if(!itemDetail.strMeasure7.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure7)
                    }
                    if(!itemDetail.strMeasure8.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure8)
                    }
                    if(!itemDetail.strMeasure9.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure9)
                    }
                    if(!itemDetail.strMeasure10.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure10)
                    }
                    if(!itemDetail.strMeasure11.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure11)
                    }
                    if(!itemDetail.strMeasure12.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure12)
                    }
                    if(!itemDetail.strMeasure13.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure13)
                    }
                    if(!itemDetail.strMeasure14.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure14)
                    }
                    if(!itemDetail.strMeasure15.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure15)
                    }
                    if(!itemDetail.strMeasure16.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure16)
                    }
                    if(!itemDetail.strMeasure17.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure17)
                    }
                    if(!itemDetail.strMeasure18.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure18)
                    }
                    if(!itemDetail.strMeasure19.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure19)
                    }
                    if(!itemDetail.strMeasure20.isNullOrEmpty()){
                        strMeasure.add(itemDetail.strMeasure20)
                    }
                    binding.rvIngredient.adapter = DetailAdapter(strIngredient)
                    binding.rvMeasurements.adapter = DetailAdapter(strMeasure)
                    binding.cl.visibility=View.GONE
                })
            }

        }
    }
}