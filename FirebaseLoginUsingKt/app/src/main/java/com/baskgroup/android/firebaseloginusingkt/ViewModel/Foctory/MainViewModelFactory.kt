package com.baskgroup.android.firebaseloginusingkt.ViewModel.Foctory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.baskgroup.android.firebaseloginusingkt.ViewModel.ViewModel.MainViewModel

class MainViewModelFactory(): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MainViewModel::class.java)){
            return MainViewModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}