package com.baskgroup.android.firebaseloginusingkt.Repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.baskgroup.android.firebaseloginusingkt.Data.Item
import com.baskgroup.android.firebaseloginusingkt.Data.ItemDetail
import com.baskgroup.android.firebaseloginusingkt.Remote.Remote
import com.baskgroup.android.firebaseloginusingkt.Utility.Utility
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService

class Repository {
    var item: ArrayList<Item> = ArrayList()
     fun getData(): ArrayList<Item> {
         val itemValue=ArrayList<Item>()
             val remote=Remote()
             val valueGet: String? =remote.requestDesert(Utility.host+"api/json/v1/1/filter.php?c=Dessert")
             val value:JSONObject  = JSONObject(valueGet)

             val valueArr:JSONArray =value.getJSONArray("meals")
             for (i in 0 until valueArr.length()) {
                 val mealObject:JSONObject = valueArr.get(i) as JSONObject
                 val name :String= mealObject.get("strMeal") as String
                 val image:String= mealObject.get("strMealThumb") as String
                 val id:String= mealObject.get("idMeal") as String
                 Log.d("Value",name)
                 itemValue.add(Item(name,image,id))
             }
//             itemValueMulti.value=itemValue
        return itemValue
    }
    fun getDataDetail(id:String): ItemDetail {
        val remote=Remote()

        val valueGet: String? =remote.requestDesert(Utility.host+"api/json/v1/1/lookup.php?i="+id)
        val value:JSONObject  = JSONObject(valueGet)
        val valueArr:JSONArray =value.getJSONArray("meals")
//        for (i in 0 until valueArr.length()) {
            val mealObject:JSONObject = valueArr.get(0) as JSONObject
            val idMeal :String= mealObject.get("idMeal").toString()
            val strMeal:String= mealObject.get("strMeal").toString()
            val strDrinkAlternate:String= mealObject.get("strDrinkAlternate").toString()
            val strCategory :String= mealObject.get("strCategory").toString()
            val strArea:String= mealObject.get("strArea").toString()
            val strInstructions:String= mealObject.get("strInstructions").toString()
            val strMealThumb :String= mealObject.get("strMealThumb").toString()
            val strTags:String= mealObject.get("strTags").toString()
            val strYoutube:String= mealObject.get("strYoutube").toString()
            val strIngredient1 :String= mealObject.get("strIngredient1").toString()
            val strIngredient2:String= mealObject.get("strIngredient2").toString()
            val strIngredient3:String= mealObject.get("strIngredient3").toString()
            val strIngredient4 :String= mealObject.get("strIngredient4").toString()
            val strIngredient5:String= mealObject.get("strIngredient5").toString()
            val strIngredient6:String= mealObject.get("strIngredient6").toString()
            val strIngredient7:String= mealObject.get("strIngredient7").toString()
            val strIngredient8 :String= mealObject.get("strIngredient8").toString()
            val strIngredient9:String= mealObject.get("strIngredient9").toString()
            val strIngredient10:String= mealObject.get("strIngredient10").toString()
            val strIngredient11 :String= mealObject.get("strIngredient11").toString()
            val strIngredient12:String= mealObject.get("strIngredient12").toString()
            val strIngredient13:String= mealObject.get("strIngredient13").toString()
            val strIngredient14 :String= mealObject.get("strIngredient14").toString()
            val strIngredient15:String= mealObject.get("strIngredient15").toString()
            val strIngredient16:String= mealObject.get("strIngredient16").toString()
            val strIngredient17:String= mealObject.get("strIngredient17").toString()
            val strIngredient18 :String= mealObject.get("strIngredient18").toString()
            val strIngredient19:String= mealObject.get("strIngredient19").toString()
            val strIngredient20:String= mealObject.get("strIngredient20").toString()
            val strMeasure1:String= mealObject.get("strMeasure1").toString()
            val strMeasure2:String= mealObject.get("strMeasure2").toString()
            val strMeasure3:String= mealObject.get("strMeasure3").toString()
            val strMeasure4:String= mealObject.get("strMeasure4").toString()
            val strMeasure5:String= mealObject.get("strMeasure5").toString()
            val strMeasure6:String= mealObject.get("strMeasure6").toString()
            val strMeasure7:String= mealObject.get("strMeasure7").toString()
            val strMeasure8:String= mealObject.get("strMeasure8").toString()
            val strMeasure9:String= mealObject.get("strMeasure9").toString()
            val strMeasure10:String= mealObject.get("strMeasure10").toString()
            val strMeasure11:String= mealObject.get("strMeasure11").toString()
            val strMeasure12:String= mealObject.get("strMeasure12").toString()
            val strMeasure13:String= mealObject.get("strMeasure13").toString()
            val strMeasure14:String= mealObject.get("strMeasure14").toString()
            val strMeasure15:String= mealObject.get("strMeasure15").toString()
            val strMeasure16:String= mealObject.get("strMeasure16").toString()
            val strMeasure17:String= mealObject.get("strMeasure17").toString()
            val strMeasure18:String= mealObject.get("strMeasure18").toString()
            val strMeasure19:String= mealObject.get("strMeasure19").toString()
            val strMeasure20:String= mealObject.get("strMeasure20").toString()
            val strSource:String= mealObject.get("strSource").toString()
            val strImageSource:String= mealObject.get("strImageSource").toString()
            val strCreativeCommonsConfirmed:String= mealObject.get("strCreativeCommonsConfirmed").toString()
            val dateModified:String= mealObject.get("dateModified").toString()
        Log.d("strIngredient10", strIngredient10)
             val itemDetail=ItemDetail(idMeal,strMeal,strDrinkAlternate,strCategory,strArea,strInstructions,strMealThumb,
                strTags,strYoutube,strIngredient1,strIngredient2,strIngredient3,strIngredient4,strIngredient5,strIngredient6,
                strIngredient7,strIngredient8,strIngredient9,strIngredient10,strIngredient11,strIngredient12,
                strIngredient13,strIngredient14,strIngredient15,strIngredient16,strIngredient17,strIngredient18,
                strIngredient19,strIngredient20,strMeasure1,strMeasure2,strMeasure3,strMeasure4,strMeasure5,strMeasure6,
                        strMeasure7,strMeasure8,strMeasure9,strMeasure10,strMeasure11,strMeasure12,strMeasure13,
                strMeasure14,strMeasure15,strMeasure16,strMeasure17,strMeasure18,strMeasure19,strMeasure20,
                strSource,strImageSource,strCreativeCommonsConfirmed,dateModified)

//        }
//             itemValueMulti.value=itemValue
        return itemDetail
    }

}