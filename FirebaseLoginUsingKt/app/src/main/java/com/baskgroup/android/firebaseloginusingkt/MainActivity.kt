package com.baskgroup.android.firebaseloginusingkt

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.baskgroup.android.firebaseloginusingkt.Adapter.ItemAdapter
import com.baskgroup.android.firebaseloginusingkt.Utility.Utility
import com.baskgroup.android.firebaseloginusingkt.ViewModel.Foctory.MainViewModelFactory
import com.baskgroup.android.firebaseloginusingkt.ViewModel.ViewModel.MainViewModel
import com.baskgroup.android.firebaseloginusingkt.databinding.ActivityMainBinding
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var firebaseAuth: FirebaseAuth
    val Req_Code: Int = 123
    var startForResult: ActivityResultLauncher<Intent>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        FirebaseApp.initializeApp(this)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        firebaseAuth = FirebaseAuth.getInstance()

        binding.btnLogin.setOnClickListener { view: View? ->
            if (binding.btnLogin.text.equals("Login")){
                signInGoogle()
            }
            else if(binding.btnLogin.text.equals("Logout")){
//                Toast.makeText(this,"Logging Out..",Toast.LENGTH_SHORT).show()
                logOut();
            }

        }
         startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult())
        { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(result.data)
                handleResult(task)
                //  you will get result here in result.data
            }

        }

    }

    private fun signInGoogle() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent

        startForResult?.launch(signInIntent)
//        startActivityForResult(signInIntent, Req_Code)
    }

    // onActivityResult() function : this is where
    // we provide the task and data for the Google Account
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == Req_Code) {
//            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
//            handleResult(task)
//        }
//    }

    private fun handleResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            if (account != null) {
                getStatus(account)
            }
        } catch (e: ApiException) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
        }
    }


    private fun getStatus(account: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                binding.btnLogin.text="Logout"
                Toast.makeText(this,"Login Successful",Toast.LENGTH_SHORT).show()
                if(Utility.isOnline(this)==true){
                    loadData();
                }else{
                    Toast.makeText(this,"Network Error",Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (GoogleSignIn.getLastSignedInAccount(this) != null) {
            binding.btnLogin.text="Logout"
            if(Utility.isOnline(this)==true){
                loadData();
            }else{
                Toast.makeText(this,"Network Error",Toast.LENGTH_SHORT).show()
            }

        }
    }
     fun  logOut() {
         firebaseAuth.signOut();
         mGoogleSignInClient.signOut().addOnCompleteListener{
             binding.btnLogin.text="Login"
             this@MainActivity.runOnUiThread(java.lang.Runnable {
                 Toast.makeText(this,"LogOut Successful",Toast.LENGTH_SHORT).show()
                 binding.rv.adapter=null
             })

         }
    }
    fun loadData(){
        // this creates a vertical layout Manager
        binding.rv.layoutManager = LinearLayoutManager(this)
        val factory = MainViewModelFactory()
        val viewModel : MainViewModel
        viewModel = ViewModelProvider(this,factory).get(MainViewModel::class.java)
        val backgroundExecutor: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
        binding.cl.visibility=View.VISIBLE
         backgroundExecutor.execute {
             viewModel.getData()?.let {
                 val adapter = ItemAdapter(viewModel.getData())
                 this@MainActivity.runOnUiThread(java.lang.Runnable {
                     binding.rv.adapter = adapter
                     binding.cl.visibility=View.GONE
                 })
             }


         }

    }

}